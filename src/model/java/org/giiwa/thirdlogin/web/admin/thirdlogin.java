package org.giiwa.thirdlogin.web.admin;

import org.giiwa.app.web.admin.setting;
import org.giiwa.core.bean.X;
import org.giiwa.core.conf.Global;

public class thirdlogin extends setting {

  @Override
  public void get() {
    this.set("page", "/admin/setting.thirdlogin.html");
  }

  @Override
  public void set() {
    Global.setConfig("thirdlogin.sina.on", X.isSame("on", this.getString("sina_on")) ? 1 : 0);
    Global.setConfig("thirdlogin.alipay.on", X.isSame("on", this.getString("alipay_on")) ? 1 : 0);
    Global.setConfig("thirdlogin.qq.on", X.isSame("on", this.getString("qq_on")) ? 1 : 0);
    Global.setConfig("thirdlogin.github.on", X.isSame("on", this.getString("github_on")) ? 1 : 0);
    Global.setConfig("thirdlogin.wechat.on", X.isSame("on", this.getString("wechat_on")) ? 1 : 0);

    Global.setConfig("thirdlogin.github.appid", this.getString("github_clientid"));
    Global.setConfig("thirdlogin.github.secret", this.getString("github_secret"));

    Global.setConfig("thirdlogin.wechat.appid", this.getString("wechat_appid"));
    Global.setConfig("thirdlogin.wechat.secret", this.getString("wechat_secret"));

    Global.setConfig("thirdlogin.sina.appid", this.getString("sina_appkey"));
    Global.setConfig("thirdlogin.sina.secret", this.getString("sina_secret"));

    Global.setConfig("thirdlogin.qq.appid", this.getString("qq_appid"));
    Global.setConfig("thirdlogin.qq.appkey", this.getString("qq_appkey"));

    Global.setConfig("thirdlogin.alipay.appid", this.getString("alipay_appid"));
    Global.setConfig("thirdlogin.alipay.app_private_key", this.getHtml("alipay_app_private_key"));
    Global.setConfig("thirdlogin.alipay.alipay_public_key", this.getHtml("alipay_alipay_public_key"));

    get();
  }

}
